import static org.junit.Assert.*;

public class Test {

	@org.junit.Test()
	public void initialisationSoldeNegatif() throws Exception {
		Compte c1 = new Compte(5);
		assertTrue(c1.getSolde()>0);
	}

	/*@org.junit.Test
	public void consultationSoldeNegatif() {
		Compte c1 = new Compte(5);
		assertTrue(c1.getSolde()>0);
	}*/

	@org.junit.Test
	public void debiterCompteAvecSoldeNegatif() throws Exception {
		Compte c1 = new Compte(5);
		assertTrue(c1.debiter(1));
	}

	@org.junit.Test
	public void debiterCompteAvecSommeSuperieurSolde() throws Exception {
		Compte c1 = new Compte(5);
		assertTrue(c1.debiter(5));
	}

	@org.junit.Test
	public void debiterCompteAvecSommeNegatif() throws Exception {
		Compte c1 = new Compte(6);
		assertTrue(c1.debiter(5));
	}
	
	@org.junit.Test(expected = Exception.class)
	public void VirementAvecUnDesCompteNULL() throws Exception {
		Compte c1 = null;
		Compte c2 = new Compte(5);
		assertTrue(c1.Virement(c2,3));
	}
	@org.junit.Test
	public void VirementSommeNegatif() throws Exception {
		Compte c1 = new Compte(5);
		Compte c2 = new Compte(5);
		assertTrue(c1.Virement(c2,3));
	}

	@org.junit.Test
	public void VirementSommeSuperieurSolde() throws Exception {
		Compte c1 = new Compte(5);
		Compte c2 = new Compte(5);
		assertTrue(c1.Virement(c2,0));
	}
	
	@org.junit.Test (expected = Exception.class)
	public void CrediterSommeNegatif() throws Exception {
		Compte c1 = new Compte(-45);
		assertTrue(c1.Crediter(5));
	}
	
	@org.junit.Test (expected = Exception.class)
	public void CrediterCompteNULL() throws Exception {
		Compte c1 = null;
		assertTrue(c1.Crediter(5));
	}
}
