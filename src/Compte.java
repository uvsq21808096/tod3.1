
public class Compte {
	private float solde;

	public Compte (float solde) throws Exception
	{
		if ((solde < 0)) 
		{
			throw new Exception("Solde n�gatif");
		}
		else 
		{
			this.solde=solde;
		}
	}

	/*public float Consultation() throws Exception
	{
		return this.getSolde();
	}*/

	public boolean debiter(float val) throws Exception
	{
		if ((val<0) || (this.solde<0) || (this.getSolde()-val<0))
		{
			throw new Exception();

		}
		else 
		{
			this.solde=this.solde-val;
			return true;
		}
	}

	public boolean Virement(Compte c, float val) throws Exception
	{
		if((val<0) || ((this.getSolde()-val)<0) || (c.equals(null)) || this.equals(null))
		{
			throw new Exception("Operation impossible");
		}
		else 
		{
			c.setSolde(c.getSolde()+val);
			this.setSolde(this.getSolde()-val);
			return true;
		}

	}

	public boolean Crediter(float val) throws Exception
	{
		if((val<0) || this.equals(null))
		{
			throw new Exception("Compte NULL");
		}
		else 
		{
			this.setSolde(this.getSolde()+val);
			return true;
		}
	}
	
	public float getSolde() {
		return solde;
	}
	
	public void setSolde(float solde) {
		this.solde = solde;
	}
}
